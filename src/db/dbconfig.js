const { MongoClient } = require("mongodb");

// const connectionURL =
//   "mongodb+srv://myby-dbmng-qadm:Mybyqadm20@myby-qa-ezh3l.mongodb.net/myby-qa?ssl=true&authSource=admin&retryWrites=true&w=majority";

const connectionURL =
  "mongodb+srv://myby-dbmng-devadm:Mybydevadm01@myby-dev-ezh3l.mongodb.net/myby-dev?ssl=true&authSource=admin&retryWrites=true&w=majority";

var _db;

module.exports = {
  connectToServer: function (callback) {
    MongoClient.connect(connectionURL, { useUnifiedTopology: true }, function (
      err,
      client
    ) {
      // console.log("mongoclient =====>>>", client);
      _db = client.db();
      return callback(err);
      // try {

      // } catch {
      //   console.log("PLEASE whitelist your IP in mongo cluster");
      //   return callback(err);
      // }
    });
  },

  getDb: function () {
    return _db;
  },
};
