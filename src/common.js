const getStartAndEndDate = async () => {
  const start = new Date();
  start.setHours(0, 0, 0, 0);
  const end = new Date();
  end.setHours(23, 59, 59, 999);
  return { start, end };
};

const genAdjustMentId = async () => {
  const day = new Date().getDate();
  const month = new Date().getMonth() + 1;
  const randNum = new Date().getTime() % 10000000;
  const finalRandNum = `${day}${month}${randNum}${Math.floor(
    Math.random() * 999
  )}`;
  return "ADJUST" + finalRandNum;
};

module.exports = { getStartAndEndDate, genAdjustMentId };
