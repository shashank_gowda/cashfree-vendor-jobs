const {
  getSettledAmountFromVendorLedger,
} = require("../calls/getSettledAmountFromVendorLedger");
const {
  updateVendorBalnceCreditAmount,
} = require("./updateVendorBalanceCreditAmount");

const callApi = async (settlementVendorId, payId, lastReturnId, order) => {
  try {
    const data = await getSettledAmountFromVendorLedger(
      settlementVendorId,
      payId,
      lastReturnId
    );
    console.log("Ledger Data", data);

    return await updateVendorBalnceCreditAmount(order, data);
  } catch (error) {
    console.log(error);
  }
};

module.exports = { callApi };
