const { cashfreeVendorLedgerCount } = require("../queries");
const { getSettledOrders } = require("./getSettledOrders");

const initVendorBalnaceCreditCheck = async () => {
  const limit = 50;
  const setteledOrdersCount = await cashfreeVendorLedgerCount();
  const quot = parseInt(setteledOrdersCount / limit);
  const rem = parseInt(setteledOrdersCount % limit);
  let totalIteration;
  if (rem) {
    totalIteration = quot + 1;
  } else {
    totalIteration = quot;
  }
  const skips = 0;
  await getSettledOrders(totalIteration, limit, skips, setteledOrdersCount);
};

module.exports = { initVendorBalnaceCreditCheck };
