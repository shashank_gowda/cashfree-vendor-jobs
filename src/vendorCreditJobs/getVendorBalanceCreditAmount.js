const { vendorBankDetailsCln } = require("../queries");
const { callApi } = require("./callApi");

const getVendorBalanceCreditAmount = async (setteledOrders) => {
  console.log(
    "debug",
    `vendor balance credit amout check  for ${
      setteledOrders.length
    } orders on.....${new Date()}`
  );
  if (setteledOrders.length) {
    setteledOrders.map(async (order) => {
      const bankDetails = await vendorBankDetailsCln(order.vendorId);
      const settlementVendorId = bankDetails && bankDetails.settlementVendorId;
      const isImportTransaction =
        order && order.paymentDetail && order.paymentDetail.importTxnId;
      let payId, lastReturnId;
      if (isImportTransaction) {
        payId = order.paymentDetail.importTxnId;
      } else {
        payId = order.paymentDetail.payOrderId;
      }
      if (order.paymentDetail.vendorLedgerLastReturnId) {
        lastReturnId = order.paymentDetail.vendorLedgerLastReturnId;
      }
      return await callApi(settlementVendorId, payId, lastReturnId, order);
    });
  }
};

module.exports = { getVendorBalanceCreditAmount };
