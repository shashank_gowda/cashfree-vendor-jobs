const {
  updateLastReturnId,
  vendorCreditUpdate,
  findOrderId,
  vendorBankDetailsCln,
} = require("../queries");
const { callApi } = require("./callApi");

const updateVendorBalnceCreditAmount = async (order, data) => {
  console.log("data inside updateVendorBalnceCreditAmount ===>>", data);
  const orderId = order.orderId;
  if (data == "REMOVE_LAST_RETURNID") {
    await updateLastReturnId(orderId);
    return;
  }

  let updateInfo;
  if (data && data.lastReturnId) {
    updateInfo = {
      "paymentDetail.vendorLedgerLastReturnId": data.lastReturnId,
    };
  } else if (data && data.eventType && data.amount) {
    updateInfo = {
      "paymentDetail.vendorCreditedAmount": data.amount,
      "paymentDetail.vendorAmountCreditedOn": data.addedOn,
    };
  }
  const res = await vendorCreditUpdate(orderId, updateInfo);

  if (res && data.lastReturnId) {
    const order = await findOrderId(orderId);
    const lastReturnIdInDB =
      order && order.paymentDetail.vendorLedgerLastReturnId;

    if (lastReturnIdInDB) {
      const bankDetails = await vendorBankDetailsCln(order.vendorId);
      const settlementVendorId = bankDetails && bankDetails.settlementVendorId;
      const isImportTransaction =
        order && order.paymentDetail && order.paymentDetail.importTxnId;
      let payId;
      if (isImportTransaction) {
        payId = order.paymentDetail.importTxnId;
      } else {
        payId = order.paymentDetail.payOrderId;
      }
      setTimeout(async () => {
        await callApi(settlementVendorId, payId, lastReturnIdInDB, order);
      }, 300000);
    }
  }
};

module.exports = { updateVendorBalnceCreditAmount };
