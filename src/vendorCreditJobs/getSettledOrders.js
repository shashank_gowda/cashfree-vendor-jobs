const { settledOrders } = require("../queries");
const {
  getVendorBalanceCreditAmount,
} = require("./getVendorBalanceCreditAmount");

const getSettledOrders = async (
  totalIteration,
  limit,
  skips,
  setteledOrdersCount
) => {
  const setteledOrders = await settledOrders(limit, skips);

  await getVendorBalanceCreditAmount(setteledOrders);
  totalIteration = totalIteration - 1;
  if (totalIteration != 0) {
    skips = skips + 1;
    setTimeout(async () => {
      await getSettledOrders(totalIteration, limit, skips, setteledOrdersCount);
    }, 300000);
  } else if (totalIteration == 0) {
    console.log(
      "debug",
      `vendor balance credit amout check for ${setteledOrdersCount} orders completed on.....${new Date()}`
    );
    return;
  }
};

module.exports = { getSettledOrders };
