const axios = require("axios");
// const { authorize } = require("./authorize");

async function verifyToken(token) {
  // const token = await authorize();

  var config = {
    method: "post",
    url: "https://ces-api.cashfree.com/ces/v1/verifyToken",
    headers: { Authorization: `Bearer ${token}` },
  };

  try {
    const res = await axios(config);
    return res.data;
  } catch (error) {
    console.log(error);
  }
}

module.exports = { verifyToken };
