const axios = require("axios");
const { authorize } = require("./authorize");
const { verifyToken } = require("./verifyToken");

//get refunds and vendor adjustments from the cashfree ledger

const getIndexOfTopVendorPayout = (indexOfOrder, ordersLedger) => {
  console.log("inside ==>>> getIndexOfTopVendorPayout");
  if (indexOfOrder !== -1) {
    for (let i = indexOfOrder; i >= 0; i--) {
      if (ordersLedger[i].eventTag == "VENDOR_PAYOUT") {
        return i;
      }
    }
    return false;
  }
  return false;
};

const getIndexOfBottomVendorPayout = (indexOfOrder, ordersLedger) => {
  console.log("inside ==>>> getIndexOfBottomVendorPayout");
  if (indexOfOrder !== -1) {
    for (let i = indexOfOrder; i <= ordersLedger.length; i++) {
      if (ordersLedger[i].eventTag == "VENDOR_PAYOUT") {
        return i;
      }
    }
    return false;
  }
  return false;
};

const filterAdjustmentOrdersFromPayouts = (
  indexOftopVendorPayout,
  indexOfbottomVendorPayout,
  ordersLedger
) => {
  console.log(
    "filterAdjustmentOrdersFromPayouts ===>",
    indexOftopVendorPayout,
    indexOfbottomVendorPayout,
    ordersLedger.length
  );
  const adjustments = [];

  for (let i = indexOftopVendorPayout; i <= indexOfbottomVendorPayout; i++) {
    const eventTag = ordersLedger[i].eventTag;
    if (eventTag == "MERCHANT_ADJUSTMENT" || eventTag == "MERCHANT_REFUND") {
      adjustments.push(ordersLedger[i]);
    }
  }

  // console.log("adjustments ===>>>>", adjustments);
  return adjustments;
};

const getAdjustmentOrders = (
  orderId,
  ordersLedger,
  lastReturnId,
  settlementVendorId
) => {
  orderId = "1287898586+5+56534";
  const indexOfOrder = ordersLedger.findIndex((order) =>
    order.remarks.includes(orderId)
  );

  const indexOftopVendorPayout = getIndexOfTopVendorPayout(
    indexOfOrder,
    ordersLedger
  );

  const indexOfbottomVendorPayout = getIndexOfBottomVendorPayout(
    indexOfOrder,
    ordersLedger
  );

  console.log("indexOfOrder ======>", indexOfOrder);

  console.log("indexOfTopVendorPayout =======>", indexOftopVendorPayout);

  console.log("indexOfBottomVendorPayout =======>", indexOfbottomVendorPayout);

  //if order is not found in the ledger get the next 200 records from ledger (user lastreturnId)

  if (
    indexOfOrder == -1 ||
    indexOftopVendorPayout == false ||
    indexOfbottomVendorPayout == false
  ) {
    console.log(
      `vendor adjustments not found!! fetching next 200 records lastreturnId: ${lastReturnId}`
    );

    getVendorAdjustments(
      orderId,
      settlementVendorId,
      lastReturnId,
      ordersLedger
    );
    return;
  }

  const adjustMentOrders = filterAdjustmentOrdersFromPayouts(
    indexOftopVendorPayout,
    indexOfbottomVendorPayout,
    ordersLedger
  );

  console.log("adjustMentOrders   =======>", adjustMentOrders);
};

const getVendorAdjustments = async (
  orderId,
  settlementVendorId,
  lastReturnId,
  ordersLedger
) => {
  console.log(
    `Inside vendor adjustments orderId: ${orderId} settlementVendorId: ${settlementVendorId} lastReturnId: ${lastReturnId}`
  );

  const token = await authorize();
  const verify = await verifyToken(token);

  if (verify.subCode == "200") {
    let ledgerData = ordersLedger ? ordersLedger : null;
    let url = `https://ces-api.cashfree.com/ces/v1/getVendorLedger/${settlementVendorId}?maxReturn=200`;
    url = lastReturnId ? url + `&lastReturnId=${lastReturnId}` : url;

    let config = {
      method: "get",
      url: url,
      headers: { Authorization: `Bearer ${token}` },
    };

    try {
      const res = await axios(config);
      // console.log("getVendorAdjustments response ===>>", res.data);
      const responseData = res.data.data;
      lastReturnId = res.data.lastReturnId;
      // console.log("responseData =====>", responseData);

      ledgerData = ordersLedger
        ? ledgerData.concat(responseData)
        : responseData;

      console.log("Ledger data ===>>", ledgerData[0]);

      getAdjustmentOrders(
        orderId,
        ledgerData,
        lastReturnId,
        settlementVendorId
      );
    } catch (error) {
      console.log(
        `Error in calling getVendorAdjustments for vendor:${settlementVendorId}`
      );
    }
  }
};

module.exports = { getVendorAdjustments };

// algo: get next 200 records from last return id
//1. Order didn't found in first set of ledger data
//2. Upper payout didn't found
//3. Lower payout didn't found
