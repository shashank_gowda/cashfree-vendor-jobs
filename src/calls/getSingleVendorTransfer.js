const axios = require("axios");
const { authorize } = require("./authorize");
const { verifyToken } = require("./verifyToken");
const { getVendorAdjustments } = require("./getVendorAdjustments");
const getSingleVendorTransfer = async (vendorTransferId) => {
  // console.log(
  //   "getSingleVendorTransfer vendorTransferId ===>>",
  //   vendorTransferId
  // );

  const token = await authorize();
  const verify = await verifyToken(token);

  if (verify.subCode == "200") {
    let url = `https://ces-api.cashfree.com/ces/v1/getVendorTransfer/${vendorTransferId}`;
    let config = {
      method: "get",
      url: url,
      headers: { Authorization: `Bearer ${token}` },
    };

    // console.log("getSingleVendorTransfer config ===>>>", config);

    try {
      const res = await axios(config);
      const vendorTransfer = res.data;
      // console.log("getSingleVendorTransfer response ===>>", res.data);
      return await vendorTransfer;
    } catch (error) {
      console.log(
        `Error in calling getSingleVendorTransfer for vendor:${settlementVendorId}`
      );
    }
  }
};

const insertVendorTransfers = async (
  vendorTransfersArray,
  vendorId,
  settlementVendorId
) => {
  if (vendorTransfersArray && vendorTransfersArray.length) {
    // N352200479607482
    try {
      // const database = db.getDb();
      // const vendorTransfersCln = await database.collection("vendorTransfers");
      let filteredVendorTransfers = vendorTransfersArray.filter(
        (vend) =>
          vend.utr == "N018210497136802" &&
          settlementVendorId == "VEN2553229542566"
      );
      // N354200480279381
      filteredVendorTransfers =
        filteredVendorTransfers[filteredVendorTransfers.length - 1];
      console.log(
        "filteredVendorTransfers ===>>>",
        filteredVendorTransfers,
        settlementVendorId
      );
      const vendorTransfer = await getSingleVendorTransfer(
        filteredVendorTransfers.vendorTransferId
      );
      const { orders, vendorId } = vendorTransfer.data;

      const firstOrderId = orders[0];

      console.log("vendor Transfer", vendorTransfer);
      console.log("vendor Transfer orders ===>>", firstOrderId);

      getVendorAdjustments(firstOrderId, vendorId);
      // await filteredVendorTransfers.forEach((vendTrans) => {
      //   if (
      //     vendTrans.utr == "00000" &&
      //     settlementVendorId == "VEN1472459559620"
      //   )
      //     console.log("vendTrans ===>>>", vendTrans, settlementVendorId);
      //   // vendorTransfersCln.insertOne({
      //   //   ...vendTrans,
      //   //   VendorTransferId: genVendorTransferId(),
      //   //   vendorId,
      //   //   settlementVendorId,
      //   //   createdDate: new Date(),
      //   //   createdBy: "System",
      //   // });
      // });
      console.log(
        `${vendorTransfersArray.length} vendorTransfers inserted for vendorId: ${vendorId} settlementVendorId: ${settlementVendorId}`
      );
    } catch (error) {
      console.log(error);
    }
  } else {
    console.log("There are no latest vendor transfers");
  }
};

module.exports = { getSingleVendorTransfer, insertVendorTransfers };

//refund total + service tax
