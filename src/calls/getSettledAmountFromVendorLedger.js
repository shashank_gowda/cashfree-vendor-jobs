const { authorize } = require("./authorize");
const { verifyToken } = require("./verifyToken");
const axios = require("axios");

const getSettledAmountFromVendorLedger = async (
  settlementVendorId,
  payId,
  lastReturnId
) => {
  // console.log("SettlementVendorId", settlementVendorId);
  // console.log("payId", payId);
  // console.log("lastReturnId", lastReturnId);
  const token = await authorize();
  const verify = await verifyToken(token);

  if (verify.subCode == "200") {
    let url = lastReturnId
      ? `https://ces-api.cashfree.com/ces/v1/getVendorLedger/${settlementVendorId}?maxReturn=200&lastReturnId=${lastReturnId}`
      : `https://ces-api.cashfree.com/ces/v1/getVendorLedger/${settlementVendorId}?maxReturn=200`;

    let config = {
      method: "get",
      url: url,
      headers: { Authorization: `Bearer ${token}` },
    };

    // console.log(config);

    try {
      const res = await axios(config);

      // console.log(res.data);

      const { status, subCode } = res.data;

      if (status == "SUCCESS" && subCode == "200") {
        const newLastReturnId = res.data.lastReturnId;
        const ledgerData = res.data.data;
        //termination case

        console.log("lastReturnId", lastReturnId);
        console.log("newLastReturnId", newLastReturnId);
        if (lastReturnId && lastReturnId === newLastReturnId) {
          console.log(`${settlementVendorId} reached end of the ledger`);
          return "REMOVE_LAST_RETURNID";
        }

        console.log(
          `settlementVendorId ${settlementVendorId}  ledger data length ===>`,
          ledgerData.length
        );
        //find the order in ledger
        const creditInfo = ledgerData.find(
          (data) => data.eventType == "CREDIT" && data.remarks.includes(payId)
        );
        console.log("creditInfo ====>>>>>>>", creditInfo, payId);
        if (creditInfo) {
          return creditInfo;
        } else {
          return { lastReturnId: newLastReturnId };
        }
      }
    } catch (error) {
      console.log("Error in calling getVendorLedger for order :", payId, error);
    }
  }
};

module.exports = { getSettledAmountFromVendorLedger };
