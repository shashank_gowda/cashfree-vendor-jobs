var axios = require("axios");
const { paymentKeys } = require("../queries");

const authorize = async () => {
  const keys = await paymentKeys();
  // console.log(keys);
  var config = {
    method: "post",
    url: "https://ces-api.cashfree.com/ces/v1/authorize",
    headers: {
      "X-Client-Id": keys.cashfree.cashfreeMPS.clientId,
      "X-Client-Secret": keys.cashfree.cashfreeMPS.clientSecret,
    },
  };

  try {
    const res = await axios(config);
    return res.data.data.token;
  } catch (error) {
    console.log(error);
  }
};

module.exports = { authorize };
