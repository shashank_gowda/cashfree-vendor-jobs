const axios = require("axios");
const { authorize } = require("./authorize");
const { verifyToken } = require("./verifyToken");
const fs = require("fs");

const getVendorLedger = async (lastReturnId) => {
  //paste settlemet vendorId
  const settlementVendorId = "";
  console.log(
    `Inside getVendorLedger settlementVendorId: ${settlementVendorId}`
  );

  const token = await authorize();
  const verify = await verifyToken(token);

  if (verify.subCode == "200") {
    let url = `https://ces-api.cashfree.com/ces/v1/getVendorLedger/${settlementVendorId}?maxReturn=200`;
    url = lastReturnId ? url + `&lastReturnId=${lastReturnId}` : url;

    let config = {
      method: "get",
      url: url,
      headers: { Authorization: `Bearer ${token}` },
    };

    try {
      const res = await axios(config);
      // console.log("getVendorAdjustments response ===>>", res.data);
      const responseData = res.data.data;
      lastReturnId = res.data.lastReturnId;
      // console.log("responseData =====>", responseData);
      const ledgerData = responseData;
      console.log("responseData =====>", ledgerData.length);

      const info = {
        lastReturnId,
      };
      const writeData = JSON.stringify(info);

      fs.readFile(
        "/Users/admin/Desktop/Trikaya_ledger/vendorLedgerLasReturnId.json",
        "utf-8",
        (err, data) => {
          if (err) {
            throw err;
          }

          // parse JSON object
          const res = data && JSON.parse(data.toString());

          // print JSON object
          console.log("res =>", res);
          if (!res || (res && res.lastReturnId !== lastReturnId)) {
            fs.writeFile(
              "/Users/admin/Desktop/Trikaya_ledger/vendorLedgerLasReturnId.json",
              writeData,
              (err) => {
                if (err) {
                  throw err;
                }
                console.log("last returnId   is saved.");

                // console.log("Ledger data ===>>", ledgerData);
                writeLedgerDataToFile(ledgerData);
                getVendorLedger(lastReturnId);
              }
            );
          } else {
            console.log("end of the ledger reached.");
          }
        }
      );
    } catch (error) {
      console.log(
        `Error in calling getVendorAdjustments for vendor:${settlementVendorId}`
      );
    }
  }
};

const writeLedgerDataToFile = (ledgerData) => {
  ledgerData = JSON.stringify(ledgerData);
  fs.appendFile(
    "/Users/admin/Desktop/Trikaya_ledger/TrikayaLedger.json",
    ledgerData,
    (err) => {
      if (err) {
        throw err;
      }
      console.log("TrikayaLedger data append success");
      // console.log("Ledger data ===>>", ledgerData);
    }
  );
};

module.exports = { getVendorLedger };
