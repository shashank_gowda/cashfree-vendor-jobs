var axios = require("axios");

const getOrderSettlementStatus = async (token, orderId) => {
  var config = {
    method: "get",
    url: `https://ces-api.cashfree.com/ces/v1/getOrderSettlementStatus/${orderId}`,
    headers: { Authorization: `Bearer ${token}` },
  };

  try {
    const res = await axios(config);
    // console.log(res);
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

module.exports = { getOrderSettlementStatus };
