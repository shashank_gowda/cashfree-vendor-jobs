const {
  vendorBankDetails,
  vendorBankDetailsCln,
  onlineOrderExistsForVendor,
  vendorTransfersExists,
  updateLastReturnIdToVendorBankDetails,
  vendorBankDetailsCount,
} = require("../queries");
const { authorize } = require("../calls/authorize");
const { verifyToken } = require("../calls/verifyToken");
const axios = require("axios");
const { insertVendorTransfers } = require("../calls/getSingleVendorTransfer");

const vendorTransfers = {};

vendorTransfers.initVendorTransfers = async () => {
  const limit = 50;
  const vendorBankCount = await vendorBankDetailsCount();
  const quot = parseInt(vendorBankCount / limit);
  const rem = parseInt(vendorBankCount % limit);
  let totalIteration;
  if (rem) {
    totalIteration = quot + 1;
  } else {
    totalIteration = quot;
  }
  const skips = 0;

  await vendorTransfers.getVendorBankDetails(
    totalIteration,
    limit,
    skips,
    vendorBankCount
  );
};

vendorTransfers.getVendorBankDetails = async (
  totalIteration,
  limit,
  skips,
  vendorBankCount
) => {
  let vendorBankDtls = await vendorBankDetails(limit, skips);
  // console.log("vendorBankdtls ===>>".vendorBankDtls.length);
  vendorBankDtls = [
    {
      _id: "YPWDrT9QWXssSXDyj",
      vendorId: "hazWNFQJXNzeeoXTu",
      settlementVendorId: "VEN2553229542566",
      name: "Murtuza",
      phone: "9880953850",
      email: "vendor_payments@preceptlabs.com",
      bankName: "AXIS BANK",
      bankAccount: "910020034660861",
      accountHolder: "TRIKAYA AGRICULTURE PVT LTD",
      ifsc: "UTIB0000182",
      address1: "No b1001 Gr regent park",
      city: "Bangalore ",
      state: "Karnataka ",
      pincode: "560083",
      commisionPercent: 100,
      vendorAdded: true,
      createdBy: "J9vxpSYAaKchQPCZ2",
    },
  ];

  await vendorTransfers.getVendorTransfers(vendorBankDtls);
  totalIteration = totalIteration - 1;
  if (totalIteration != 0) {
    skips = skips + 1;
    setTimeout(async () => {
      await vendorTransfers.getVendorBankDetails(
        totalIteration,
        limit,
        skips,
        vendorBankCount
      );
    }, 300000);
  } else if (totalIteration == 0) {
    console.log(
      "debug",
      `vendor transfers  check for ${vendorBankCount} orders completed on.....${new Date()}`
    );
    return;
  }
};

vendorTransfers.getVendorTransfers = async (vendorBankDtls) => {
  console.log(
    "debug",
    `vendor transfers for ${vendorBankDtls.length} vendors on.....${new Date()}`
  );

  if (vendorBankDtls.length) {
    vendorBankDtls.forEach(async (bank) => {
      const vendorId = bank.vendorId;
      // const onlineOrderExists = await onlineOrderExistsForVendor(vendorId);
      if ((onlineOrderExists = true)) {
        let lastReturnId;
        const bankDetails = await vendorBankDetailsCln(vendorId);
        const transferExists = await vendorTransfersExists(vendorId);

        const settlementVendorId =
          bankDetails && bankDetails.settlementVendorId;

        if (bank.vendorTransfersLastReturnId) {
          lastReturnId = bank.vendorTransfersLastReturnId;
        }

        return await vendorTransfers.callApi(
          settlementVendorId,
          vendorId,
          lastReturnId,
          transferExists,
          bank
        );
      }
    });
  }
};

vendorTransfers.callApi = async (
  settlementVendorId,
  vendorId,
  lastReturnId,
  transferExists,
  bank
) => {
  settlementVendorId = "VEN2553229542566";
  vendorId = "hazWNFQJXNzeeoXTu";
  try {
    const vendorTrnasfersData = await vendorTransfers.getVendorTransfersApi(
      settlementVendorId,
      lastReturnId,
      transferExists
    );

    // console.log("vendor transfers Data", vendorTrnasfersData);

    // return await updateVendorBalnceCreditAmount(order, data);
    return await vendorTransfers.updateVendorTransfersAndBankDetails({
      ...vendorTrnasfersData,
      vendorId,
      settlementVendorId,
    });
  } catch (error) {
    console.log(error);
  }
};

vendorTransfers.updateVendorTransfersAndBankDetails = async (
  vendorTrnasfersData
) => {
  const {
    lastReturnId,
    vendorTransfers,
    vendorId,
    settlementVendorId,
  } = vendorTrnasfersData;

  await insertVendorTransfers(vendorTransfers, vendorId, settlementVendorId);

  if (vendorTransfers.length >= 200) {
    //for pagination
    await updateLastReturnIdToVendorBankDetails(lastReturnId, vendorId);
  }
};

vendorTransfers.getFormatedDate = (date) => {
  const getYear = new Date(date).getFullYear();
  const getMonth = new Date(date).getMonth();
  const getDate = new Date(date).getDate();
  return `${getYear}${getMonth}${getDate}`;
};

vendorTransfers.getPath = (
  settlementVendorId,
  lastReturnId,
  transferExists
) => {
  let path;
  if (transferExists) {
    //  get the latest vendor transfers
    const lastAddedOn = vendorTransfers.getFormatedDate(transferExists.addedOn);
    const todaysDate = vendorTransfers.getFormatedDate(new Date());

    path = `https://ces-api.cashfree.com/ces/v1/getVendorTransfers/${settlementVendorId}?maxReturn=200&startDate=${lastAddedOn}&endDate=${todaysDate}`;
  } else {
    //TODO: use last return id to fetch the more (>200) records
    path = `https://ces-api.cashfree.com/ces/v1/getVendorTransfers/${settlementVendorId}?maxReturn=200`;
  }
  console.log("path ====>>", path);
  return path;
};

vendorTransfers.getVendorTransfersApi = async (
  settlementVendorId,
  lastReturnId,
  transferExists
) => {
  console.log("settlementVendorId ===>", settlementVendorId);

  const token = await authorize();
  const verify = await verifyToken(token);

  if (verify.subCode == "200") {
    let url = vendorTransfers.getPath(
      settlementVendorId,
      lastReturnId,
      transferExists
    );

    let config = {
      method: "get",
      url: url,
      headers: { Authorization: `Bearer ${token}` },
    };

    console.log("config ===>>>", config);

    try {
      const res = await axios(config);
      // console.log(res.data);
      const { status, subCode } = res.data;
      const newLastReturnId = res.data.lastReturnId;
      const vendorTransfers = res.data.data;
      if (status == "SUCCESS" && subCode == "200") {
        return { vendorTransfers, lastReturnId: newLastReturnId };
      }
      //   const newLastReturnId = res.data.lastReturnId;
      //   const ledgerData = res.data.data;
      //   //termination case

      //   console.log("lastReturnId", lastReturnId);
      //   console.log("newLastReturnId", newLastReturnId);
      //   if (lastReturnId && lastReturnId === newLastReturnId) {
      //     console.log(`${settlementVendorId} reached end of the ledger`);
      //     return "REMOVE_LAST_RETURNID";
      //   }

      //   console.log(
      //     `settlementVendorId ${settlementVendorId}  ledger data length ===>`,
      //     ledgerData.length
      //   );
      //   //find the order in ledger
      //   const creditInfo = ledgerData.find(
      //     (data) => data.eventType == "CREDIT" && data.remarks.includes(payId)
      //   );
      //   console.log("creditInfo ====>>>>>>>", creditInfo, payId);
      //   if (creditInfo) {
      //     return creditInfo;
      //   } else {
      //     return { lastReturnId: newLastReturnId };
      //   }
      // }
    } catch (error) {
      console.log(
        `Error in calling getVendorTransfersApi for vendor:${settlementVendorId}`
      );
    }
  }
};

// vendorTransfers.filterDuplicateSettelementVendorIds = async () => {
//   const database = db.getDb();
//   const vendorBankDetailsCln = await database.collection("vendorBankDetails");
//   const dupFilterdVendorBanks = [];
//   vendorBankDetailsCln
//     .aggregate([
//       {
//         $group: {
//           _id: { projectId: "$settlementVendorId" },
//           dups: { $addToSet: "$_id" },
//           count: { $sum: 1 },
//         },
//       },
//       {
//         $match: {
//           count: { $gt: 1 },
//         },
//       },
//     ])
//     .forEach(function (doc) {
//       doc.dups.shift();
//       if (!doc.dups.includes(doc._id)) {
//         dupFilterdVendorBanks.push(doc);
//       }
//       // db.usersProject.remove({
//       //     _id: {$in: doc.dups}
//       // });
//     });
//   console.log("dupFilterdVendorBanks ===>>", dupFilterdVendorBanks);
// };
module.exports = { vendorTransfers };
