const { settlementStatusUpdate } = require("../queries");

const updateSettlementStatus = async (order, data) => {
  // console.log("order===>>>", order);
  // console.log("data===>>>", data);
  if (data.vendorTransfers && data.vendorTransfers.length) {
    try {
      const vendorTransfer = data.vendorTransfers[0];
      const vendorTransferStatus = await vendorTransfer.status;
      if (vendorTransferStatus == "SUCCESS" && data.subCode == "200") {
        const result = await settlementStatusUpdate(
          order.orderId,
          vendorTransfer.totalAmount,
          vendorTransfer.addedOn
        );

        if (result) {
          console.log("status updated for order ===>>", order.orderId);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }
};

module.exports = { updateSettlementStatus };
