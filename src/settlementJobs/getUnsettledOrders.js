const { unsettledOrders } = require("../queries");
const { getSettlementStatus } = require("./getSettlementStatus");

const getUnsettledOrders = async (
  totalIteration,
  limit,
  skips,
  unSetteledOrdersCount
) => {
  const unSetteledOrders = await unsettledOrders(limit, skips);

  await getSettlementStatus(unSetteledOrders, totalIteration);
  totalIteration = totalIteration - 1;
  if (totalIteration != 0) {
    skips = skips + 1;
    setTimeout(async () => {
      await getUnsettledOrders(
        totalIteration,
        limit,
        skips,
        unSetteledOrdersCount
      );
    }, 90000);
  } else if (totalIteration == 0) {
    return;
  }
};

module.exports = { getUnsettledOrders };
