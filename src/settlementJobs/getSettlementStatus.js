const {
  getOrderSettlementStatus,
} = require("../calls/getOrderSettlementStatus");
const { authorize } = require("../calls/authorize");
const { verifyToken } = require("../calls/verifyToken");
const { updateSettlementStatus } = require("./updateSettlementStatus");

const getSettlementStatus = async (unSetteledOrders, totalIteration) => {
  if (unSetteledOrders.length) {
    console.log(
      "debug",
      `vendor settlement status check for ${
        unSetteledOrders.length
      } orders iteration : ${totalIteration} on.....${new Date()}`
    );

    unSetteledOrders.forEach(async (order) => {
      const isImportTransaction =
        order && order.paymentDetail && order.paymentDetail.importTxnId;
      let payId;
      if (isImportTransaction) {
        payId = order.paymentDetail.importTxnId;
      } else {
        payId = order.paymentDetail.payOrderId;
      }

      const token = await authorize();
      const verifiedToken = await verifyToken(token);
      if (verifiedToken.subCode == "200") {
        const settlementStatus = await getOrderSettlementStatus(token, payId);

        console.log(token);
        console.log("verifiedToken", verifiedToken);
        console.log(settlementStatus);
        await updateSettlementStatus(order, settlementStatus);
      }
    });
  }
};

module.exports = { getSettlementStatus };
