const { cashfreeSettlementQuery } = require("../queries");

const { getUnsettledOrders } = require("./getUnsettledOrders");

const initVendorSettlementStatusCheck = async () => {
  const limit = 90;
  const unSetteledOrdersCount = await cashfreeSettlementQuery();
  const quot = parseInt(unSetteledOrdersCount / limit);
  const rem = parseInt(unSetteledOrdersCount % limit);
  let totalIteration;
  if (rem) {
    totalIteration = quot + 1;
  } else {
    totalIteration = quot;
  }
  const skips = 0;
  await getUnsettledOrders(totalIteration, limit, skips, unSetteledOrdersCount);
};

module.exports = { initVendorSettlementStatusCheck };
