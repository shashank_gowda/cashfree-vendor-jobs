const {
  cancelledAndRejectedOrders,
  myOrdersRefundUpdate,
  refundPaymentUpdate,
} = require("../queries");
const { getRefundProcessedStatus } = require("./getRefundProcessedStatus");

const getCancelledAndRejectedOrders = async () => {
  const cancelledAndRejectedPaidOrders = await cancelledAndRejectedOrders();
  console.log("cancelled orders ===>", cancelledAndRejectedPaidOrders);
  console.log(
    "debug",
    `Refund processed status check for ${
      cancelledAndRejectedPaidOrders.length
    } orders on.....${new Date()}`
  );
  if (
    cancelledAndRejectedPaidOrders &&
    !cancelledAndRejectedPaidOrders.length
  ) {
    return;
  }
  cancelledAndRejectedPaidOrders.map(async (order) => {
    const res = await getRefundProcessedStatus(order);
    const newRefundInfo = res.refund[0];
    newRefundInfo.processed = "YES";
    if (newRefundInfo.processed == "YES") {
      console.log(
        "debug",
        `order with refundId:${
          newRefundInfo.refundId
        } is processed on... ${new Date(newRefundInfo.processedOn)}`
      );

      await myOrdersRefundUpdate(
        newRefundInfo.refundId,
        newRefundInfo.processed,
        newRefundInfo.processedOn
      );
      await refundPaymentUpdate(
        newRefundInfo.refundId,
        newRefundInfo.processed,
        newRefundInfo.processedOn
      );
    }
  });
};

module.exports = { getCancelledAndRejectedOrders };
