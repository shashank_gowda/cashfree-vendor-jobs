var axios = require("axios");
var qs = require("qs");

async function getRefundProcessedStatus(order) {
  var data = qs.stringify({
    appId: "3528754946a62033fb053cea8253",
    secretKey: "297e826938a919367eb7a94dcf8ec202e09acade",
    refundId: order.paymentDetail.refundId,
  });

  var config = {
    method: "post",
    url: "https://test.cashfree.com/api/v1/refundStatus",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Cache-Control": "no-cache",
    },
    data: data,
  };

  try {
    const res = await axios(config);
    if (res.data.status == "OK") {
      return res.data;
    }
  } catch (error) {
    console.log(error.response.data);
  }
}

module.exports = { getRefundProcessedStatus };
