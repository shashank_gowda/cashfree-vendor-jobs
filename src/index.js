const express = require("express");
const cron = require("node-cron");
const {
  initVendorSettlementStatusCheck,
} = require("./settlementJobs/initVendorSettlementStatusCheck");
const {
  getCancelledAndRejectedOrders,
} = require("./refundJobs/getCancelledAndRejectedOrders");
const {
  initVendorBalnaceCreditCheck,
} = require("./vendorCreditJobs/initVendorBalanceCreditCheck");
const { vendorTransfers } = require("./vendorTransfers/vendorTransfers");
var mongoUtil = require("./db/dbconfig");
const { getVendorLedger } = require("./calls/getVendorLedger");

const app = express();

mongoUtil.connectToServer(function (err, client) {
  if (err) console.log(err);

  try {
    // cron.schedule("0 0 */1 * *", async () => {
    //   console.log("refund status check job Running...", new Date());
    //   await getCancelledAndRejectedOrders();
    // });
    // cron.schedule("0 0 */1 * *", async () => {
    //   console.log("Settlement status check job Running...", new Date());
    //   await initVendorSettlementStatusCheck();
    // });
    // cron.schedule("0 0 */1 * *", async () => {
    //   console.log(
    //     "Vendor Balance Credit status check job Running...",
    //     new Date()
    //   );
    //   await initVendorBalnaceCreditCheck();
    // });
    // cron.schedule("* * * * *", async () => {
    //   console.log("Vendor Transfers status check job Running...", new Date());
    //   await vendorTransfers.initVendorTransfers();
    // });
    // cron.schedule("0 0 */1 * *", async () => {
    //   console.log("get Vendor Ledger...", new Date());
    //   await getVendorLedger();
    // });
  } catch (error) {
    console.log(error);
  }
});

app.listen(8010, () => {
  console.log("*****----- Server on port 8010 -----*****", new Date());
});
