const db = require("./db/dbconfig");
const { getStartAndEndDate } = require("./common");

const paymentKeys = async () => {
  try {
    const database = db.getDb();
    const paymentKeysCollection = await database.collection("paymentKeys");
    return await paymentKeysCollection.findOne({
      provider: "cashfree",
    });
  } catch (err) {
    console.log(err);
  }
};

const cancelledAndRejectedOrders = async () => {
  const { start, end } = await getStartAndEndDate();

  try {
    const database = db.getDb();
    const ordersCollection = await database.collection("my_orders");
    return await ordersCollection
      .find(
        {
          createdDate: {
            $gte: start,
            $lte: end,
          },
          $or: [
            { "statusInfo.status": "Cancelled" },
            { "statusInfo.status": "Rejected" },
          ],
          paymentStatus: "Paid",
          "paymentDetail.refundProcessed": "NO",
        },
        {
          fields: {
            paymentDetail: 1,
            _id: 1,
          },
        }
      )
      .toArray();
  } catch (error) {
    console.log(error);
  }
};

const cashfreeSettlementQuery = async () => {
  try {
    const database = db.getDb();
    const ordersCollection = await database.collection("my_orders");
    return await ordersCollection
      .find({
        "statusInfo.status": "Completed",
        paymentStatus: "Paid",
        "paymentDetail.vendorSettlement": "NO",
        "paymentDetail.status": "Payment success",
      })
      .count();
  } catch (error) {
    console.log(error);
  }
};

const cashfreeVendorLedgerCount = async () => {
  try {
    const database = db.getDb();
    const ordersCollection = await database.collection("my_orders");
    return await ordersCollection
      .find({
        "statusInfo.status": "Completed",
        paymentStatus: "Paid",
        "paymentDetail.vendorSettlement": "YES",
        "paymentDetail.status": "Payment success",
        "paymentDetail.vendorCreditedAmount": { $exists: false },
      })
      .count();
  } catch (error) {
    console.log(error);
  }
};

const settledOrders = async (limit, skips) => {
  try {
    const database = db.getDb();
    const ordersCollection = await database.collection("my_orders");
    return await ordersCollection
      .find(
        {
          "statusInfo.status": "Completed",
          paymentStatus: "Paid",
          "paymentDetail.vendorSettlement": "YES",
          "paymentDetail.status": "Payment success",
          "paymentDetail.vendorCreditedAmount": { $exists: false },
        },
        { skip: limit * skips, limit: limit }
      )
      .toArray();
  } catch (error) {
    console.log(error);
  }
};

const unsettledOrders = async (limit, skips) => {
  try {
    const database = db.getDb();
    const ordersCollection = await database.collection("my_orders");
    return await ordersCollection
      .find(
        {
          "statusInfo.status": "Completed",
          paymentStatus: "Paid",
          "paymentDetail.vendorSettlement": "NO",
          "paymentDetail.status": "Payment success",
        },
        { skip: limit * skips, limit: limit }
      )
      .toArray();
  } catch (error) {
    console.log(error);
  }
};

const settlementStatusUpdate = async (orderId, totalAmount, addedOn) => {
  try {
    const database = db.getDb();
    const ordersCollection = await database.collection("my_orders");
    return await ordersCollection.update(
      {
        orderId: orderId,
      },
      {
        $set: {
          "paymentDetail.vendorSettlement": "YES",
          "paymentDetail.vendorSettledAmount": totalAmount,
          "paymentDetail.vendorAmountSettledOn": addedOn,
          updatedDate: new Date(),
          updatedBy: "System",
        },
      }
    );
  } catch (error) {
    console.log(error);
  }
};

const myOrdersRefundUpdate = async (refundId, processed, processedOn) => {
  try {
    const database = db.getDb();
    const ordersCollection = await database.collection("my_orders");
    return await ordersCollection.update(
      { "paymentDetail.refundId": refundId },
      {
        $set: {
          "paymentDetail.refundProcessed": processed,
          "paymentDetail.refundProcessedOn": new Date(processedOn),
          updatedBy: "System",
          updatedDate: new Date(),
        },
        $push: {
          statusList: {
            id: 6,
            name: "Refund Processed",
            createdBy: "System",
            createdDate: new Date(),
          },
        },
      }
    );
  } catch (error) {
    console.log(error);
  }
};

const refundPaymentUpdate = async (refundId, processed, processedOn) => {
  try {
    const database = db.getDb();
    const ordersCollection = await database.collection("my_orders");
    return await ordersCollection.update(
      { refundId: refundId },
      {
        $set: {
          "statusList.3.refund.0.processed": processed,
          "statusList.3.refund.0.processedOn": processedOn,
          updatedBy: "System",
          updatedDate: new Date(),
        },
      }
    );
  } catch (error) {
    console.log(error);
  }
};

const vendorBankDetailsCln = async (vendorId) => {
  try {
    const database = db.getDb();
    const vendorBankDetailsCollection = await database.collection(
      "vendorBankDetails"
    );
    return await vendorBankDetailsCollection.findOne({
      vendorId: vendorId,
    });
  } catch (error) {
    console.log(error);
  }
};

const vendorBankDetailsCount = async () => {
  try {
    const database = db.getDb();
    const vendorBankDetailsCollection = await database.collection(
      "vendorBankDetails"
    );
    return await vendorBankDetailsCollection
      .find({
        vendorAdded: true,
      })
      .count();
  } catch (error) {
    console.log(error);
  }
};

const vendorBankDetails = async (limit, skips) => {
  try {
    const database = db.getDb();
    const vendorBankDetailsCollection = await database.collection(
      "vendorBankDetails"
    );
    return await vendorBankDetailsCollection
      .aggregate([
        // {
        //   $group: {
        //     _id: { vendorId: "$vendorId" },
        //     dups: { $addToSet: "$_id" },
        //     count: { $sum: 1 },
        //   },
        // },

        {
          $match: {
            vendorAdded: true,
            // count: { $gt: 1 },
          },
        },
        { $skip: limit * skips },
        { $limit: limit },
      ])
      .toArray();
    // .map((doc) => {
    //   doc.dups.shift();
    //   if (doc.dups.includes(doc._id)) {

    //     return doc;
    //   }
    // });
  } catch (error) {
    console.log(error);
  }
};

const onlineOrderExistsForVendor = async (vendorId) => {
  try {
    const database = db.getDb();
    const ordersCollection = await database.collection("my_orders");
    const counts = await ordersCollection
      .find({
        vendorId: vendorId,
        paymentStatus: "Paid",
        "paymentDetail.status": "Payment success",
      })
      .count();

    if (counts) {
      return true;
    }
  } catch (error) {
    console.log(error);
  }
};

const vendorTransfersExists = async (vendorId) => {
  try {
    const database = db.getDb();
    const vendorTransfersCollection = await database.collection(
      "vendorTransfers"
    );
    const vendorTransfer = await vendorTransfersCollection
      .find({
        vendorId: vendorId,
      })
      .sort({ addedOn: -1 })
      .limit(1);

    if (vendorTransfer) {
      return vendorTransfer.addedOn;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
  }
};

const updateLastReturnId = async (orderId) => {
  try {
    const database = db.getDb();
    const ordersCollection = await database.collection("my_orders");
    return await ordersCollection.update(
      {
        orderId: orderId,
      },
      {
        $unset: {
          "paymentDetail.vendorLedgerLastReturnId": 1,
        },
      }
    );
  } catch (error) {
    console.log(error);
  }
};

const vendorCreditUpdate = async (orderId, updateInfo) => {
  try {
    const database = db.getDb();
    const ordersCollection = await database.collection("my_orders");
    return await ordersCollection.update(
      {
        orderId: orderId,
      },
      {
        $set: {
          ...updateInfo,
          updatedDate: new Date(),
          updatedBy: "System",
        },
      }
    );
  } catch (error) {
    console.log(error);
  }
};

const findOrderId = async (orderId) => {
  try {
    const database = db.getDb();
    const myOrdersCollection = await database.collection("my_orders");
    return await myOrdersCollection.findOne({
      orderId: orderId,
    });
  } catch (error) {
    console.log(error);
  }
};

const genVendorTransferId = () => {
  //vendorTransferId generation

  const day = new Date().getDate();
  const month = new Date().getMonth() + 1;
  const randNum = new Date().getTime() % 10000000;
  const finalRandNum = `${day}${month}${randNum}${Math.floor(
    Math.random() * 999
  )}`;
  return "VEN_TRANS" + finalRandNum;
};

// const insertVendorTransfers = async (
//   vendorTransfersArray,
//   vendorId,
//   settlementVendorId
// ) => {
//   if (vendorTransfersArray && vendorTransfersArray.length) {
//     try {
//       const database = db.getDb();
//       const vendorTransfersCln = await database.collection("vendorTransfers");
//       const filteredVendorTransfers = vendorTransfersArray.filter(
//         (vend) =>
//           vend.utr == "00000" && settlementVendorId == "VEN1472459559620"
//       )[0];
//       console.log(
//         "filteredVendorTransfers ===>>>",
//         filteredVendorTransfers,
//         settlementVendorId
//       );
//       await getSingleVendorTransfer(filteredVendorTransfers.vendorTransferId);

//       // await filteredVendorTransfers.forEach((vendTrans) => {
//       //   if (
//       //     vendTrans.utr == "00000" &&
//       //     settlementVendorId == "VEN1472459559620"
//       //   )
//       //     console.log("vendTrans ===>>>", vendTrans, settlementVendorId);
//       //   // vendorTransfersCln.insertOne({
//       //   //   ...vendTrans,
//       //   //   VendorTransferId: genVendorTransferId(),
//       //   //   vendorId,
//       //   //   settlementVendorId,
//       //   //   createdDate: new Date(),
//       //   //   createdBy: "System",
//       //   // });
//       // });
//       console.log(
//         `${vendorTransfersArray.length} vendorTransfers inserted for vendorId: ${vendorId} settlementVendorId: ${settlementVendorId}`
//       );
//     } catch (error) {
//       console.log(error);
//     }
//   } else {
//     console.log("There are no latest vendor transfers");
//   }
// };

const updateLastReturnIdToVendorBankDetails = async (
  lastReturnId,
  vendorId
) => {
  if (lastReturnId) {
    try {
      const database = db.getDb();
      const vendorBankDetailsCln = await database.collection(
        "vendorBankDetails"
      );
      await vendorBankDetailsCln.updateOne(
        { vendorId },
        { $set: { lastReturnId, updatedDate: new Date(), updatedBy: "System" } }
      );
      console.log(
        `lastReturnId: ${lastReturnId} for vendor:${vendorId} updated`
      );
    } catch (error) {
      console.log(error);
    }
  } else {
    console.log(`lastReturnId update is not required for vendor:${vendorId}`);
  }
};

module.exports = {
  paymentKeys,
  cashfreeSettlementQuery,
  cancelledAndRejectedOrders,
  unsettledOrders,
  settledOrders,
  settlementStatusUpdate,
  myOrdersRefundUpdate,
  refundPaymentUpdate,
  cashfreeVendorLedgerCount,
  vendorBankDetailsCln,
  updateLastReturnId,
  vendorCreditUpdate,
  findOrderId,
  vendorBankDetailsCount,
  vendorBankDetails,
  onlineOrderExistsForVendor,
  vendorTransfersExists,
  // insertVendorTransfers,
  updateLastReturnIdToVendorBankDetails,
};
